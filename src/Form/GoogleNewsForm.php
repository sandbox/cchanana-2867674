<?php

namespace Drupal\googlenews\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Google_Analytics settings for this site.
 */
class GoogleNewsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'googlenews_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['googlenews_admin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('googlenews_admin.settings');
    $node_types = node_type_get_names();
    
    $form['help'] = [
      '#markup' => '<p>' . t('Settings for controlling the <a href="@news-sitemap">Google News sitemap file</a>.', ['@news-sitemap' => '/googlenews.xml']),
    ];
    
    $form['count'] = [
    '#markup' => '<p>' . t('There are currently @count node(s) suitable for output.', ['@count' => count(googlenews_list_nodes()) != '' ? count(googlenews_list_nodes()) : 0]) . "</p>\n",
    ];
    $form['googlenews_publication_name'] = [
    '#type' => 'textfield',
    '#title' => t('Publication name'),
    '#default_value' => $config->get(googlenews_publication_name),
    '#description' => t("Leave blank to use the site's name instead: :site_name", [':site_name' => \Drupal::config('system.site')->get('name')]),
    ];
    
    $form['googlenews_node_types'] = [
      '#type' => 'checkboxes',
      '#title' => t('Select the content types to include'),
      '#default_value' => $config->get('googlenews_node_types') != '' ? $config->get('googlenews_node_types') : array_keys($node_types),
      '#options' => $node_types,
    ];
    
    $form['googlenews_cache_timeout'] = [
      '#type' => 'textfield',
      '#title' => t('Cache timeout (minutes)'),
      '#default_value' => $config->get('googlenews_cache_timeout') != '' ? $config->get('googlenews_cache_timeout') : '15',
      '#description' => t('The number of minutes that the sitemap file will be cached for before it is regenerated.'),
    ];
    
    $form['googlenews_content_hours'] = [
      '#type' => 'textfield',
      '#title' => t('Maximum content age (hours)'),
      '#default_value' => intval($config->get('googlenews_content_hours') != '' ? $config->get('googlenews_content_hours') : '48'),
      '#description' => t('All content (nodes) created within this number of hours will be included in the sitemap file. It is recommended to leave this at the default of 48 hours.'),
    ];
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('googlenews_admin.settings');
    $config->set('googlenews_publication_name', $form_state->getValue('googlenews_publication_name'));
    $config->set('googlenews_node_types', $form_state->getValue('googlenews_node_types'));
    $config->set('googlenews_cache_timeout', $form_state->getValue('googlenews_cache_timeout'));
    $config->set('googlenews_content_hours', $form_state->getValue('googlenews_content_hours'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
