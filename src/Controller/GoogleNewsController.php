<?php

  /**
   * @file
   * Contains \Drupal\googlenews\src\Controller\GoogleNewsController.
   */
  namespace Drupal\googlenews\Controller;

  use Drupal\Core\Controller\ControllerBase;
  use Drupal\node\Entity\Node;
  use Drupal\Core\Url;

  /**
   * Controller routines for products routes.
   */
  class GoogleNewsController extends ControllerBase {

    public function getgooglenews() {
      $negotiator = \Drupal::service('domain.negotiator');
      $domainID = $negotiator->getActiveDomain()->id();
      $cid = $domainID.'-googlenews';
      $content = '';
      
      if ($cache =  \Drupal::cache()->get($cid)) {
        // Verify the data hasn't expired.
        if (time() < $cache->expire) {
          $content = $cache->data;
        }
      }
      // If nothing loaded from the cache, build it now.
      if (empty($content)) {
        $config = \Drupal::config('googlenews_admin.settings');
        $publication_name = $config->get('googlenews_publication_name');

        $content = '<?xml version="1.0" encoding="UTF-8"?>';
      $content .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9">';
      if(googlenews_list_nodes()){
      foreach (googlenews_list_nodes() as $record) {
        // Load the node.
        $node = Node::load($record->nid);
        $langcode = $node->language()->getId();
        $options = array('absolute' => TRUE);
        $url_obj = Url::fromRoute('entity.node.canonical', ['node' => $record->nid], $options);
        $url_string = $url_obj->toString();
        $content .= '<url>';
        $content .= '<loc>'.$url_string.'</loc>';
        $content .= '<news:news>';
        $content .= '<news:publication>';
        $content .= '<news:name>' . $publication_name . '</news:name>';
        $content .= '<news:language>' . $langcode . '</news:language>';
        $content .= '</news:publication>';
        $content .= '<news:title>'.$node->getTitle().'</news:title>';
        $content .= '<news:publication_date>'.  gmdate(DATE_W3C,$node->getCreatedTime()).'</news:publication_date>';
        $content .= '</news:news>';
        $content .= '</url>';
        }
      }
        $content .= '</urlset>';
        $timeout = time() + (intval($config->get('googlenews_cache_timeout') != '' ? $config->get('googlenews_cache_timeout') : '15') * 60);
        \Drupal::cache()->set($cid, $content, $timeout);
      }
      
      header("Content-type: text/xml");
      echo $content;
      exit();
     }
  }